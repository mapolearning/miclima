package com.mapo.miclima.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class GetLocationService extends Service {

    private static final String TAG = GetLocationService.class.getSimpleName();

    public static final int MSG_STOP_UPDATING_LOCATION = 0;
    public static final int MSG_START_UPDATING_LOCATION = 1;

    LocationManager locationManager;
    int minTime = 1000 * 60 * 2;
    MyLocationListener listener;

    public class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_STOP_UPDATING_LOCATION:
                    stopGettingLocation();
                    break;
                case MSG_START_UPDATING_LOCATION:
                    startGettingLocation();
                    break;
            }
            super.handleMessage(msg);
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public IBinder onBind(Intent intent) {
        startGettingLocation();
        return mMessenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startGettingLocation();
        return super.onStartCommand(intent, flags, startId);
    }

    private void stopGettingLocation() {
        if (locationManager != null && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            locationManager.removeUpdates(listener);
    }

    private void startGettingLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, 0, listener);
        }
    }

    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Log.d("LocationService","Location: "+location.getLatitude()+"/"+location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
