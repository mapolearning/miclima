package com.mapo.miclima.ui.activity;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.service.GetLocationService;
import com.mapo.miclima.ui.fragment.MainFragment;
import com.mapo.miclima.ui.fragment.WeatherDetailFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.MainFragmentCallback {
    private Boolean twoPane = false;
    private WeatherDetailFragment weatherDetailFragment;

    private Messenger mServiceLocation = null;
    private boolean mBoundLocation = false;

    private static final int PERMISSION_REQUEST_UPDATE_LOCATION = 100;

    private ServiceConnection mConnectionLocation = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mServiceLocation = new Messenger(service);
            mBoundLocation = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mServiceLocation = null;
            mBoundLocation = false;
        }
    };

    private void sendMessageToUpdateService(Message msg) {
        if (!mBoundLocation) return;

        try {
            mServiceLocation.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void bindLocationService() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            bindService(new Intent(MainActivity.this, GetLocationService.class), mConnectionLocation,
                    Context.BIND_AUTO_CREATE);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_UPDATE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_UPDATE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                bindLocationService();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.content_frame) != null) {
            twoPane = true;

            if (weatherDetailFragment == null) {
                weatherDetailFragment = new WeatherDetailFragment();
            }
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_frame,weatherDetailFragment,WeatherDetailFragment.TAG)
                    .commit();
        }

        MainFragment mainFragment = (MainFragment)getFragmentManager().findFragmentById(R.id.fragment_forecast);
        if (mainFragment != null) {
            mainFragment.setCallback(this);
        }

    }

    @Override
    protected void onStart() {
        bindLocationService();

        /*
        Intent intent = new Intent(this,GetLocationService.class);
        startService(intent);
        */

        super.onStart();
    }

    @Override
    protected void onResume() {
        sendMessageToUpdateService(Message.obtain(null,GetLocationService.MSG_START_UPDATING_LOCATION));
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mBoundLocation) {
            sendMessageToUpdateService(Message.obtain(null, GetLocationService.MSG_STOP_UPDATING_LOCATION));
            unbindService(mConnectionLocation);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void forecastSelected(Forecast forecast) {
        if (twoPane) {
            weatherDetailFragment.setForecast(forecast);
        } else {
            Intent intent = new Intent(this, WeatherDetailActivity.class);
            intent.putExtra(WeatherDetailFragment.FORECAST_ID,forecast.getId());
            startActivity(intent);
        }
    }
}
