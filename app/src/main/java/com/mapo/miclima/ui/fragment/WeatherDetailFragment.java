package com.mapo.miclima.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.util.WeatherUtil;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WeatherDetailFragment extends Fragment {

    public static final String TAG = WeatherDetailFragment.class.getSimpleName();
    public static final String FORECAST_ID = "forecastId";

    @Bind(R.id.dateDesc)
    TextView dateDescTxt;
    @Bind(R.id.monthDesc)
    TextView monthDescTxt;
    @Bind(R.id.maxTemp)
    TextView maxTempTxt;
    @Bind(R.id.minTemp)
    TextView minTempTxt;
    @Bind(R.id.weatherArt)
    ImageView weatherArtImage;
    @Bind(R.id.weatherDesc)
    TextView weatherDescTxt;
    @Bind(R.id.humidity)
    TextView humidityTxt;
    @Bind(R.id.pressure)
    TextView pressureTxt;
    @Bind(R.id.speed)
    TextView speedTxt;

    private Forecast forecast;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        ButterKnife.bind(this,view);

        if (savedInstanceState != null) {
            forecast = Forecast.getForecast(savedInstanceState.getLong(FORECAST_ID));
        } else {
            Bundle bundle = getArguments();
            if (bundle != null)
                forecast = Forecast.getForecast(bundle.getLong(FORECAST_ID));
        }

        refreshView();

        return view;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
        refreshView();
    }

    private void refreshView() {
        if (forecast != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.getDefault());
            monthDescTxt.setText(sdf.format(forecast.forecastDate));
            dateDescTxt.setText(forecast.getForecastDateStr());
            minTempTxt.setText(WeatherUtil.formatTemperature(getActivity(),forecast.min));
            maxTempTxt.setText(WeatherUtil.formatTemperature(getActivity(),forecast.max));
            weatherArtImage.setImageResource(forecast.weather.getArtIcon());
            weatherDescTxt.setText(forecast.weather.description);
            String humidity = String.format(getString(R.string.humidity),forecast.humidity);
            humidityTxt.setText(humidity);
            String pressure = String.format(getString(R.string.pressure),forecast.day);
            pressureTxt.setText(pressure);
            String speed = String.format(getString(R.string.wind),WeatherUtil.getFormattedWind(getActivity(),forecast.speed,forecast.deg));
            speedTxt.setText(speed);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (forecast != null)
            outState.putLong(FORECAST_ID,forecast.getId());
        super.onSaveInstanceState(outState);
    }
}
