package com.mapo.miclima.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Settings;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LocationDialog extends DialogFragment {
    public static final String TAG = LocationDialog.class.getSimpleName();

    @Bind(R.id.locationTxt)
    TextView locationTxt;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_location,(ViewGroup)getView(),false);
        ButterKnife.bind(this, dialogView);

        locationTxt.setText(Settings.getLocation(getActivity()));


        builder.setView(dialogView)
                .setTitle(R.string.app_name)
                .setPositiveButton("OK",null);

        return builder.create();
    }
}
