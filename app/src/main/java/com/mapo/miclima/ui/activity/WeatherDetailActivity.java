package com.mapo.miclima.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.ui.fragment.WeatherDetailFragment;

import butterknife.ButterKnife;


public class WeatherDetailActivity extends AppCompatActivity {

    private Forecast forecast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            forecast = Forecast.getForecast(savedInstanceState.getLong(WeatherDetailFragment.FORECAST_ID));
        } else {
            forecast = Forecast.getForecast(getIntent().getLongExtra(WeatherDetailFragment.FORECAST_ID,0));
        }

        WeatherDetailFragment weatherDetailFragment = new WeatherDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(WeatherDetailFragment.FORECAST_ID,forecast.getId());
        weatherDetailFragment.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame,weatherDetailFragment)
                .commit();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (forecast != null)
            outState.putLong(WeatherDetailFragment.FORECAST_ID,forecast.getId());
        super.onSaveInstanceState(outState);
    }
}
