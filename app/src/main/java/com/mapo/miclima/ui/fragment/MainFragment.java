package com.mapo.miclima.ui.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.content.ContentProvider;
import com.mapo.miclima.R;
import com.mapo.miclima.api.weather.WeatherApi;
import com.mapo.miclima.api.weather.WeatherClient;
import com.mapo.miclima.api.weather.response.DayForecastResponse;
import com.mapo.miclima.api.weather.response.ForecastDailyResponse;
import com.mapo.miclima.db.City;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.db.Settings;
import com.mapo.miclima.ui.adapter.ForecastCursorAdapter;
import com.mapo.miclima.ui.dialog.LocationDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int FORECAST_LOADER = 0;

    @Bind(R.id.listView)
    public ListView listView;

    private ForecastCursorAdapter forecastAdapter;
    private MainFragmentCallback callback;

    public MainFragment() {
    }

    public void setCallback(MainFragmentCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        ButterKnife.bind(this,view);

        setHasOptionsMenu(true);

        //Inicializar nuestro CursorAdapter
        Cursor cursor = Forecast.getForecastsCursor();
        forecastAdapter = new ForecastCursorAdapter(getActivity(),cursor);
        listView.setAdapter(forecastAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor selectedCursor = (Cursor) parent.getItemAtPosition(position);
                Forecast itemSelected = Forecast.toForecast(selectedCursor);

                if (callback != null)
                    callback.forecastSelected(itemSelected);
            }
        });

        //getData();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(FORECAST_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_map) {
            showLocationOnMap();
        } else if (item.getItemId() == R.id.action_dialog) {

            if (getFragmentManager().findFragmentByTag(LocationDialog.TAG) != null) {
                LocationDialog locationDialog = (LocationDialog)getFragmentManager().findFragmentByTag(LocationDialog.TAG);
                locationDialog.dismiss();
            }
            LocationDialog locationDialog = new LocationDialog();
            locationDialog.show(getFragmentManager(),LocationDialog.TAG);

            /*
            new AlertDialog.Builder(getActivity())
                    .setTitle("Mi Clima")
                    .setMessage("Soy un Dialogo.\nEs usted bien hecho?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getActivity(),"Esta rajando?",Toast.LENGTH_SHORT).show();;
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getActivity(),"Salado!",Toast.LENGTH_SHORT).show();;
                        }
                    })
                    .show();
                    */
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLocationOnMap() {
        String location = Settings.getLocation(getActivity());
        Uri geoLocation = Uri.parse("geo:0,0?").buildUpon()
                .appendQueryParameter("q",location)
                .build();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.e("Error","No se puede abrir el Mapa");
        }
    }

    /*
    private void refreshMe() {
        if (Forecast.getForecasts() != null) {
            if (forecastAdapter == null) {
                forecastAdapter = new ForecastAdapter(getActivity(), Forecast.getForecasts());
                listView.setAdapter(forecastAdapter);
            } else {
                forecastAdapter.setItems(Forecast.getForecasts());
            }
        } else {
            getData();
        }
    }
    */

    private void getData() {
        WeatherApi api = WeatherClient.getInstance().getWeatherApi();
        Call<ForecastDailyResponse> call = api.getDailyForecast(Settings.getLocation(getActivity())
                ,WeatherClient.MODE_JSON
                , Settings.getMetric(getActivity())
                ,7
                ,WeatherClient.API_KEY);
        call.enqueue(new Callback<ForecastDailyResponse>() {
            @Override
            public void onResponse(Call<ForecastDailyResponse> call, Response<ForecastDailyResponse> response) {
                if (response != null && response.body() != null) {
                    List<DayForecastResponse> forecasts = response.body().getList();
                    City city = null;
                    if (response.body().getCity() != null)
                        city = response.body().getCity().toCity();
                    if (forecasts != null && forecasts.size() > 0) {
                        for (int i = 0; i < forecasts.size(); i++) {
                            forecasts.get(i).toForecast(i, city);
                        }
                    }
                    getLoaderManager().restartLoader(FORECAST_LOADER,null,MainFragment.this);
                }
            }

            @Override
            public void onFailure(Call<ForecastDailyResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity()
                , ContentProvider.createUri(Forecast.class,null)
                ,null
                ,null
                ,null
                ,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        forecastAdapter.swapCursor(Forecast.getForecastsCursor());
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        forecastAdapter.swapCursor(null);
    }

    public interface MainFragmentCallback {
        void forecastSelected(Forecast forecast);
    }
}
