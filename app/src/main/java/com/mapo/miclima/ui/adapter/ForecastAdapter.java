package com.mapo.miclima.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapo.miclima.R;
import com.mapo.miclima.api.weather.response.DayForecastResponse;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.util.WeatherUtil;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.logging.SimpleFormatter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForecastAdapter extends ArrayAdapter<Forecast> {

    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;

    private List<Forecast> items;

    public ForecastAdapter(Context context, List<Forecast> items) {
        super(context, R.layout.item_forecast, items);
        this.items = items;
    }

    public void setItems(List<Forecast> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == VIEW_TYPE_TODAY)?VIEW_TYPE_TODAY:VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        if (this.items == null)
            return 0;
        return this.items.size();
    }

    @Override
    public Forecast getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        int layoutId;
        if (viewType == VIEW_TYPE_TODAY)
            layoutId = R.layout.item_forecast_today;
        else
            layoutId= R.layout.item_forecast;

        ViewHolder viewHolder;
        if (convertView == null || position == VIEW_TYPE_TODAY+1) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutId,parent,false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Forecast item = getItem(position);
        viewHolder.maxTemp.setText(WeatherUtil.formatTemperature(getContext(),item.max));
        viewHolder.minTemp.setText(WeatherUtil.formatTemperature(getContext(),item.min));
        viewHolder.descTxt.setText(item.weather.description);
        if (position == VIEW_TYPE_TODAY)
            viewHolder.icon.setImageResource(item.weather.getArtIcon());
        else
            viewHolder.icon.setImageResource(item.weather.getIcon());

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.minTemp)
        TextView minTemp;
        @Bind(R.id.maxTemp)
        TextView maxTemp;
        @Bind(R.id.desc)
        TextView descTxt;
        @Bind(R.id.icon)
        ImageView icon;

        public ViewHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
