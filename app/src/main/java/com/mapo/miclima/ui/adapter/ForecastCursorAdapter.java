package com.mapo.miclima.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Forecast;
import com.mapo.miclima.util.WeatherUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForecastCursorAdapter extends CursorAdapter {
    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;

    public ForecastCursorAdapter(Context context, Cursor c) {
        super(context, c, true);
    }

    @Override
    public int getItemViewType(int position) {
        return (position == VIEW_TYPE_TODAY)?VIEW_TYPE_TODAY:VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int viewType = getItemViewType(cursor.getPosition());
        int layoutId = -1;
        switch (viewType) {
            case VIEW_TYPE_TODAY: {
                layoutId = R.layout.item_forecast_today;
                break;
            }
            case VIEW_TYPE_FUTURE_DAY: {
                layoutId = R.layout.item_forecast;
                break;
            }
        }

        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Forecast item = Forecast.toForecast(cursor);

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.maxTemp.setText(WeatherUtil.formatTemperature(context,item.max));
        viewHolder.minTemp.setText(WeatherUtil.formatTemperature(context,item.min));
        viewHolder.descTxt.setText(item.weather.description);
        if (cursor.getPosition() == VIEW_TYPE_TODAY)
            viewHolder.icon.setImageResource(item.weather.getArtIcon());
        else
            viewHolder.icon.setImageResource(item.weather.getIcon());
    }

    static class ViewHolder {
        @Bind(R.id.minTemp)
        TextView minTemp;
        @Bind(R.id.maxTemp)
        TextView maxTemp;
        @Bind(R.id.desc)
        TextView descTxt;
        @Bind(R.id.icon)
        ImageView icon;

        public ViewHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
