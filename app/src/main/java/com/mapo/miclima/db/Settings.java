package com.mapo.miclima.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mapo.miclima.R;

public class Settings {

    public static String getMetric(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_units_key)
                ,context.getString(R.string.pref_unit_metric));
    }

    public static String getLocation(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_location_key)
                ,context.getString(R.string.pref_location_default));
    }
}
