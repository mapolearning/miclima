package com.mapo.miclima.db;

import android.location.Location;
import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "City", id = BaseColumns._ID)
public class City extends Model {
    @Column(name = "latitude")
    public Float latitude;
    @Column(name = "longitude")
    public Float longitude;
    @Column(name = "city")
    public String city;
    @Column(name = "country")
    public String country;

    public List<Forecast> getForecasts() {
        return getMany(Forecast.class,"city");
    }

    public static List<City> allCities() {
        return new Select()
                .from(City.class)
                .orderBy("country,name")
                .execute();
    }

    public static City getCity(String country, String city) {
        if (country != null && city != null) {
            return new Select()
                    .from(City.class)
                    .where("country = ? AND city = ?", country, city)
                    .executeSingle();
        }
        return null;
    }

    public static City newCity(String country
            , String city
            , Float latitude
            , Float longitude) {

        City theCity = getCity(country,city);
        if (theCity == null) {
            theCity = new City();
            theCity.country = country;
            theCity.city = city;
        }
        theCity.longitude = longitude;
        theCity.latitude = latitude;
        theCity.save();

        return theCity;
    }
}
