package com.mapo.miclima.db;

import android.database.Cursor;
import android.provider.BaseColumns;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.mapo.miclima.db.data.MiClimaContract;
import com.mapo.miclima.util.WeatherUtil;

import java.util.Date;
import java.util.List;

@Table(name = MiClimaContract.ForecastEntry.TABLE_NAME, id = BaseColumns._ID)
public class Forecast extends Model {
    @Column(name = MiClimaContract.ForecastEntry.COLUMN_FORECAST_ID, index = true)
    public Integer forecastId;
    @Column(name = "forecastDate")
    public Date forecastDate;
    @Column(name = "min")
    public Float min;
    @Column(name = "max")
    public Float max;
    @Column(name = "humidity")
    public Float humidity;
    @Column(name = "speed")
    public Float speed;
    @Column(name = "day")
    public Float day;
    @Column(name = "night")
    public Float night;
    @Column(name = "evening")
    public Float evening;
    @Column(name = "deg")
    public Float deg;
    @Column(name = "city")
    public City city;
    @Column(name = "weather")
    public Weather weather;

    public String getForecastDateStr() {
        return WeatherUtil.dateToString(this.forecastDate);
    }

    private static From getForecastsFrom() {
        return new Select()
                .from(Forecast.class)
                .orderBy("forecastDate DESC");
    }

    public static Cursor getForecastsCursor() {
        return Cache.openDatabase().rawQuery(getForecastsFrom().toSql(),null);
    }

    public static Forecast toForecast(Cursor cursor) {
        Forecast forecast = new Forecast();
        forecast.loadFromCursor(cursor);
        return forecast;
    }

    public static List<Forecast> getForecasts() {
        return getForecastsFrom()
                .execute();
    }

    public static Forecast getForecast(Integer forecastId) {
        return new Select()
                .from(Forecast.class)
                .where("forecastId = ?",forecastId)
                .executeSingle();
    }

    public static Forecast getForecast(Long forecastId) {
        return new Select()
                .from(Forecast.class)
                .where(BaseColumns._ID + " = ?",forecastId)
                .executeSingle();
    }

    public static Forecast newForecast(Integer forecastId, Date forecastDate, Float min, Float max, Float humidity, Float speed, Float day, Float night, Float evening, City city, Weather weather, Float deg) {
        Forecast forecast = getForecast(forecastId);
        if (forecast == null) {
            forecast = new Forecast();
            forecast.forecastId = forecastId;
        }

        forecast.forecastDate = forecastDate;
        forecast.min = min;
        forecast.max = max;
        forecast.humidity = humidity;
        forecast.speed = speed;
        forecast.day = day;
        forecast.night = night;
        forecast.evening = evening;
        forecast.city = city;
        forecast.weather = weather;
        forecast.deg = deg;
        forecast.save();

        return forecast;
    }
}
