package com.mapo.miclima.db.data;

import android.provider.BaseColumns;

public class MiClimaContract {

    public static class CityEntry implements BaseColumns {



        public static final String TABLE_NAME = "City";

        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_CITY = "city";
        public static final String COLUMN_COUNTRY = "country";

    }

    public static class WeatherEntry implements BaseColumns {

        public static final String TABLE_NAME = "Weather";

        public static final String COLUMN_WEATEHER_ID = "weatherId";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_ICON = "icon";

    }

    public static class ForecastEntry implements BaseColumns {

        public static final String TABLE_NAME = "Forecast";

        public static final String COLUMN_FORECAST_ID = "forecastId";
        public static final String COLUMN_FORECAST_DATE = "forecastDate";
        public static final String COLUMN_MIN = "min";
        public static final String COLUMN_MAX = "max";
        public static final String COLUMN_HUMIDITY = "humidity";
        public static final String COLUMN_SPEED = "speed";
        public static final String COLUMN_DAY = "day";
        public static final String COLUMN_NIGHT = "night";
        public static final String COLUMN_EVENING = "evening";
        public static final String COLUMN_DEG = "deg";

        public static final String COLUMN_CITY_ID = "city";
        public static final String COLUMN_WEATHER_ID = "weather";

    }
}
