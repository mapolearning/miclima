package com.mapo.miclima.db;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.mapo.miclima.util.WeatherUtil;

@Table(name = "Weather", id = BaseColumns._ID)
public class Weather extends Model {
    @Column(name = "weatherId")
    public Integer weatherId;
    @Column(name = "description")
    public String description;
    @Column(name = "icon")
    public String icon;

    public int getIcon() {
        return WeatherUtil.getIconResourceForWeatherCondition(this.weatherId);
    }

    public int getArtIcon() {
        return WeatherUtil.getArtResourceForWeatherCondition(this.weatherId);
    }

    public static Weather getWeather(Integer weatherId) {
        return new Select()
                .from(Weather.class)
                .where("weatherId = ?",weatherId)
                .executeSingle();
    }

    public static Weather newWeather(Integer weatherId, String description, String icon) {
        Weather weather = getWeather(weatherId);
        if (weather == null) {
            weather = new Weather();
            weather.weatherId = weatherId;
        }
        weather.description = description;
        weather.icon = icon;
        weather.save();

        return weather;
    }
}
