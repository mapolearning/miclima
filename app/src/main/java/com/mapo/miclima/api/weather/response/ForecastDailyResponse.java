package com.mapo.miclima.api.weather.response;

import java.util.List;

public class ForecastDailyResponse {
    private String cod;
    private Float message;
    private Integer cnt;
    private List<DayForecastResponse> list;
    private CityResponse city;

    public String getCod() {
        return cod;
    }

    public Float getMessage() {
        return message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public CityResponse getCity() {
        return city;
    }

    public List<DayForecastResponse> getList() {
        return list;
    }
}
