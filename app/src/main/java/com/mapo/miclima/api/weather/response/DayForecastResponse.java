package com.mapo.miclima.api.weather.response;

import com.mapo.miclima.db.City;
import com.mapo.miclima.db.Forecast;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DayForecastResponse {
    private Long dt;
    private Float pressure;
    private Float humidity;
    private Float speed;
    private Float deg;
    private Integer clouds;
    private Float rain;
    private TemperatureResponse temp;
    private List<WeatherResponse> weather;

    public Long getDt() {
        return dt;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getHumidity() {
        return humidity;
    }

    public Float getSpeed() {
        return speed;
    }

    public Float getDeg() {
        return deg;
    }

    public Integer getClouds() {
        return clouds;
    }

    public Float getRain() {
        return rain;
    }

    public TemperatureResponse getTemp() {
        return temp;
    }

    public List<WeatherResponse> getWeather() {
        return weather;
    }

    public Forecast toForecast(int position, City city) {
        DateTime forecastDate = new DateTime();

        return Forecast.newForecast(position,forecastDate.plusDays(position).toDate()
                ,temp.getMin()
                ,temp.getMax()
                ,this.humidity
                ,this.speed
                ,this.temp.getDay()
                ,this.temp.getNight()
                ,this.temp.getEve()
                ,city
                ,this.weather.get(0).toWeather()
                ,this.deg);
    }
}
