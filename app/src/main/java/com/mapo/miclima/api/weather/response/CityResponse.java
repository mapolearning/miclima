package com.mapo.miclima.api.weather.response;

import com.mapo.miclima.db.City;

public class CityResponse {
    private Integer id;
    private String name;
    private String country;
    private Integer population;
    private CoordinateResponse coord;

    public CityResponse(Integer id, String name, String country, Integer population, CoordinateResponse coord) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.population = population;
        this.coord = coord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public CoordinateResponse getCoord() {
        return coord;
    }

    public void setCoord(CoordinateResponse coord) {
        this.coord = coord;
    }

    public City toCity() {
        return City.newCity(this.country
                ,this.name
                ,this.coord.getLat()
                ,this.coord.getLon());
    }
}
