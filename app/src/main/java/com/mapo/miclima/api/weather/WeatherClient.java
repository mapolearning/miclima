package com.mapo.miclima.api.weather;


import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherClient {
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static final String API_KEY = "aae499129f082b84fe122f023b498145";
    public static final String UNIT_METRIC = "metric";
    public static final String UNIT_IMPERIAL = "imperial";
    public static final String MODE_JSON = "json";

    private static WeatherClient instance = null;

    private WeatherApi weatherApi;

    public WeatherClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        weatherApi = retrofit.create(WeatherApi.class);
    }

    public static WeatherClient getInstance() {
        if (instance == null)
            instance = new WeatherClient();
        return instance;
    }

    public WeatherApi getWeatherApi() {
        return weatherApi;
    }
}
