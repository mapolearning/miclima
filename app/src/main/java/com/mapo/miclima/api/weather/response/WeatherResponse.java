package com.mapo.miclima.api.weather.response;

import com.mapo.miclima.R;
import com.mapo.miclima.db.Weather;

public class WeatherResponse {
    private Integer id;
    private String main;
    private String description;
    private String icon;

    public Integer getId() {
        return id;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public Weather toWeather() {
        return Weather.newWeather(this.id,this.description,this.icon);
    }
}
