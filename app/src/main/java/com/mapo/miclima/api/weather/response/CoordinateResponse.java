package com.mapo.miclima.api.weather.response;

/**
 * Created by scascacha on 2/19/16.
 */
public class CoordinateResponse {
    private Float lon;
    private Float lat;

    public CoordinateResponse(Float lon, Float lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }
}
