package com.mapo.miclima.api.weather;

import com.mapo.miclima.api.weather.response.ForecastDailyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by scascacha on 2/13/16.
 */
public interface WeatherApi {
    @GET("forecast/daily")
    Call<ForecastDailyResponse> getDailyForecast(@Query("q") String query
            , @Query("mode") String mode
            , @Query("units") String units
            , @Query("cnt") Integer cnt
            , @Query("appid") String apiKey);
}
